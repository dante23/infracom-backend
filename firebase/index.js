
var admin = require("firebase-admin");

var serviceAccount = require("../config/firebase.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://ecommerce-2d0a6.firebaseio.com"
});

module.exports = admin
