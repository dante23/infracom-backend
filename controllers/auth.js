const User = require("../models/user")

exports.createOrUpdateUserController = async (req, res) => {
    console.log("get the users: ", req)
    const { email, picture, name } = req.user
    const user = await User.findOneAndUpdate({ email },{
        name, picture
    }, { new: true })

    // check if had user then update
    if (user) {
        console.log("UPDATED USER", user)
        res.json(user)
    } else { // if doesnt have user then create it
        const newUser = await new User({
            email, name, picture
        }).save()

        console.log("CREATED USER", newUser)
        res.json(newUser)
    }
}
