const express = require("express")

const routes = express.Router();

const { getUser } = require("../controllers/user")

routes.get("/users", getUser)

module.exports = routes;
