const express = require("express")

const routes = express.Router();

// middleware
const { authCheck, createOrUpdateUserMiddleware } = require("../middlewares/auth")

// controllers
const { createOrUpdateUserController } = require("../controllers/auth")

routes.post("/create-update-user", authCheck, createOrUpdateUserController)

module.exports = routes;
